import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Motion, spring} from 'react-motion';

const Beforeposts = () => (
  <div style={{ maxWidth: 500, margin: 'auto'}}>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <div className="row">
          <div className="col-xs-12
                col-sm-6
                col-md-6
                col-lg-6">
              <div style={{marginBottom: 20}}>
              <Motion defaultStyle={{o: 0}} style={{o: spring(1)}}>
                {value => <div>
                  <RaisedButton
                    style = {{ opacity: value.o }}
                    label="Account"
                    primary={true}
                    fullWidth={true}
                  />
                </div>}
              </Motion>
              </div>
          </div>
          {/* the second link */}
          <div className="col-xs-12
                col-sm-6
                col-md-6
                col-lg-6">
              <div style={{marginBottom: 20}}>
                <Motion defaultStyle={{o: 0}} style={{o: spring(1)}}>
                  {value => <div>
                    <RaisedButton
                      style = {{ opacity: value.o }}
                      label="Settings"
                      primary={true}
                      fullWidth={true}
                    />
                  </div>}
                </Motion>
              </div>
          </div>
      </div>
    </MuiThemeProvider>
  </div>
);

export default Beforeposts;
