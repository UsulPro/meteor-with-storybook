import React from 'react';
import { storiesOf } from '@kadira/storybook';
import Postslist from '../Postslist.jsx';


storiesOf('module.Postslist', module)
    .add('with data', () => {
      const posts = [
        {
          _id: 0,
          authorName: 'Tev',
          authorStatus: 'Web Programmer',
          authorAvatar: '',
          header: 'First Post',
          date: '3 days ago',
          content: 'Lorem ipsum dolor sit a  lorem erat volutpat. Nullafacilisi.',
        },
        {
          _id: 2,
          authorName: 'Tev',
          authorStatus: 'Web Developer',
          authorAvatar: '',
          header: 'Second Post',
          date: '5 days ago',
          content: 'Lorem ipsum dolor sit a  lorem erat volutpat. Nullafacilisi',
        },
        {
          _id: 3,
          authorName: 'Tev',
          authorStatus: 'Web Developer',
          authorAvatar: '',
          header: 'Third Post',
          date: '10 days ago',
          content: 'Lorem ipsum dolor sit a  lorem erat volutpat. Nullafacilisi',
        }
      ];

      return (
        <Postslist posts={posts} />
      );
    })
    .add('Without Data', () => {
      const posts = [];
      return (
        <Postslist posts={posts} />
      );
    });
