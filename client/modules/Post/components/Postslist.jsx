import React from 'react';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Transition from 'react-motion-ui-pack';
import { darkBlack} from 'material-ui/styles/colors';

// if no data
const renderIfNoDataInPosts = (posts) => {
  if (posts && posts.length === 0) {
    return (
      <List>
        <Transition
          component="div"
          enter={{
            opacity: 1,
          }}
          leave={{
            opacity: 0,
          }}
        >
          <ListItem
            leftAvatar={<Avatar>N
            </Avatar>}
            primaryText="There are no posts"
            secondaryText={
              <p>
                <span style={{color: darkBlack}}>The first one will be written soon</span>
              </p>
            }
            secondaryTextLines={2}
          />
        </Transition>
      </List>
      );
  }
};

// the Component
const Postslist = ({posts}) => (
  <div>
        <section style={{ maxWidth: '98%', margin: 'auto', position: 'relative' }}>
          <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
             <div style={{ maxWidth: 700, margin: 'auto', position: 'relative' }}>
              { renderIfNoDataInPosts(posts)}
               <List>
                 <Transition
                   component="div"
                   enter={{
                     height: 'auto',
                   }}
                   leave={{
                     height: 0,
                   }}
                 >
                 {posts.map(({_id,
                  header,
                  authorName,
                  date}) => (
                   <ListItem
                     key={_id}
                     leftAvatar={<Avatar>
                       {header.charAt(0).toUpperCase()}
                     </Avatar>}
                     primaryText={header}
                     secondaryText={
                       <p>
                         <span style={{color: darkBlack}}>{authorName}</span> --
                         {date}
                       </p>
                     }
                     secondaryTextLines={2}
                   />
                 ))}
                 </Transition>
               </List>
             </div>
          </MuiThemeProvider>
        </section>
  </div>
);

export default Postslist;
