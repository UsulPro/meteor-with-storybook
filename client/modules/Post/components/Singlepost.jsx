import React from 'react';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Paper from 'material-ui/Paper';
import Transition from 'react-motion-ui-pack';

// the singlepostcontainer-outer
const thesinglepostcontainerouter = {
  maxWidth: 700,
  margin: 'auto',
  position: 'relative',
  zIndex: 10,
  top: 100,
};

// the singlepostcontainer-inner
const singlepostcontainerinner = {
  maxWidth: '92%',
  margin: 'auto',
  position: 'relative'
};

// thebackgroundbanner
const thebackgroundbanner = {
  minWidth: '100%',
  maxWidth: '100%',
  position: 'fixed',
  backgroundColor: '#00bcd4',
  zIndex: -10,
  height: 300
};

const Singlepost = () => (
  <div>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <section>
      {/* the background */}
      <Transition
        component="div"
        enter={{
          opacity: 1,
        }}
        leave={{
          opacity: 0,
        }}
      >
      <div style={thebackgroundbanner}>
        <div style={{height: 300}} />
      </div>
      </Transition>
      {/* the content */}
      <div style={thesinglepostcontainerouter}>
        <Transition
          component="div"
          enter={{
            height: 'auto',
          }}
          leave={{
            opacity: 0,
          }}
        >
            <Paper
              style={singlepostcontainerinner}
              zDepth={5}
            >
              Hello World
            </Paper>
        </Transition>
      </div>
      </section>
    </MuiThemeProvider>
  </div>
);

export default Singlepost;
