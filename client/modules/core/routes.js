import React from 'react';
import {mount} from 'react-mounter';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

// the layouts
import Mainlayout from './components/Mainlayout.jsx';

// the components
import Home from './components/Home.jsx';

export default function (injectDeps) {
  const MainlayoutThemeWraped = (
    <MuiThemeProvider muiTheme={getMuiTheme()}>
        <Mainlayout />
    </MuiThemeProvider>
  )
  const MainLayoutCtx = injectDeps(Mainlayout);

  // Move these as a module and call this from a main file
  FlowRouter.route('/', {
    name: 'Home',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<Home />)
      });
    }
  });
}
