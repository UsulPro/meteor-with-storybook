import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Corebanner from '../Corebanner.jsx';

storiesOf('module.Corebanner', module)
  .add('default view', () => {
    return (
      <Corebanner />
    );
  })
