import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Home from '../Home.jsx';

storiesOf('module.Home', module)
  .add('default view', () => {
    return (
      <Home />
    );
  })
