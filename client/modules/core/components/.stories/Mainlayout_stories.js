import React from 'react';
import { storiesOf, addDecorator } from '@kadira/storybook';
import {muiTheme} from 'storybook-addon-material-ui';

import Component from '../Mainlayout.jsx';

storiesOf('module.Mainlayout', module)
// Add the `muiTheme` decorator to provide material-ui support to your stories.
// If you do not specify any arguments it starts with two default themes
// You can also configure `muiTheme` as a global decorator.
  .addDecorator(muiTheme())
  .add('default view', () => {
    return (
      <Component />
    );
  });
