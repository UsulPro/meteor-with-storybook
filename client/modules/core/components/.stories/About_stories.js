import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import {muiTheme} from 'storybook-addon-material-ui';
import About from '../About.jsx';

storiesOf('module.About', module)
  .addDecorator(muiTheme())
  .add('default view', () => {
    return (
      <About />
    );
  })
