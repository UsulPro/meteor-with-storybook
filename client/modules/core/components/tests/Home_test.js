const {describe, it} = global;
import {expect} from 'chai';
import {shallow} from 'enzyme';
import Home from '../Home';

describe('test', () => {
  it('should display the post title', () => {
    const el = shallow(<Home />);
    expect(el).not.to.be.null;
  });
});
