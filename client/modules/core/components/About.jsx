import React from 'react';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {Motion, spring} from 'react-motion';
const About = () => (
  <div>
    {/*<MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>*/}
      <section>
        {/* the second thing */}
        <div className="row">
        {/* the card header */}
        <div className="col-xs-12
                        col-sm-4
                        col-md-4
                        col-lg-4">
              <Motion
                defaultStyle={{o: 0, t: 100}}
                style={{o: spring(1)}}
              >
                {value => <div>
                  <Card
                    style={{
                      opacity: value.o,
                    }}>
                    <CardHeader
                      title="URL Avatar"
                      subtitle="Subtitle"
                      avatar="http://lorempixel.com/400/200"
                    />
                  </Card>
                </div>}
              </Motion>
       </div>
        {/* the card pic */}
            <div className="col-xs-12
                            col-sm-4
                            col-md-4
                            col-lg-4">
                    {/* the card pic*/}
                    <Motion
                      defaultStyle={{o: 0, d: 0}}
                      style={{o: spring(1), d: spring(Math.round(2.5))}}
                    >
                      {value => <div>
                        <Card
                          style={{ opacity: value.o }}>
                          <CardMedia
                            overlay={<CardTitle
                              title="Overlay title" subtitle="Overlay subtitle" />}
                          >
                            <img src="http://lorempixel.com/400/200" />
                          </CardMedia>
                        </Card>
                      </div>}
                    </Motion>
            </div>
            {/* the content div */}
            <div className="col-xs-12
                            col-sm-4
                            col-md-4
                            col-lg-4">
                    {/* the content */}
                    <Motion
                      defaultStyle={{o: 0, d: 1}}
                      style={{o: spring(10), d: spring(Math.round(2.5))}}
                    >
                      {value => <div>
                        <Card
                          style={{ opacity: value.o }}>
                          <CardTitle title="Card title" subtitle="Card subtitle" />
                          <CardText>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec mattis pretium massa. Aliquam erat volutpat. Nulla
                            facilisi.
                            Donec vulputate interdum sollicitudin. Nunc lacinia auctor
                            quam sed pellentesque.
                            Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis
                             odio.
                          </CardText>
                          <CardActions>
                            <FlatButton label="Action1" />
                            <FlatButton label="Action2" />
                          </CardActions>
                        </Card>
                      </div>}
                    </Motion>
            </div>
        </div>
      </section>
    {/*</MuiThemeProvider>*/}
  </div>
);
export default About;
