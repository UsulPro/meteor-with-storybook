import React from 'react';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import {Motion, spring} from 'react-motion';

const Mainlayout = ({content = () => null }) => (
  <div>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <section>
          <Motion defaultStyle={{x: 0.1}} style={{x: spring(1)}}>
            {value => <AppBar
                        title="Perspective Engine {Webdevs}"
                        showMenuIconButton={false}
                        style={{ opacity: value.x, position: 'fixed', zIndex: 10}}
                        zDepth={0}
                        id="mainlayout_background"
            /> }
          </Motion>
        </section>
    </MuiThemeProvider>
    {content()}
  </div>
);

export default Mainlayout;
