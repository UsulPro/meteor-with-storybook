import React from 'react';
import {Motion, spring, presets} from 'react-motion';

const Corebanner = () => (
  <div>
    <Motion defaultStyle={{o: 0.10, f: 2}}
       style={{o: spring(1),
        f: spring(35, presets.gentle)}}>
      {value => <div style={{opacity: value.o}}>
        <div className="row center-xs">
          <div className="col-xs-6">
              <div style={{fontSize: value.f, fontWeight: 'lighter'}}>
                  For Web Developers
              </div>
          </div>
        </div>
      </div>}
    </Motion>
    <div style={{height: 1000}} />
  </div>
);

export default Corebanner;
